# 项目代码获取:

先使用下面的链接加入项目

https://gitee.com/yangyuanhuyyh/ajtft/invite_link?invite=bb8a3bd95289665dd06e234bc9e54aa872a4ae277da93e03b7993663152babc5d1eb4c2169f92a845f318cd36bbddc3a

然后自行克隆



# 运行项目

在源代码中找到App.java 右键运行

<img src="https://gitee.com/yangyuanhuyyh/MDimage/raw/master/uPic/image-20210120173845326.png" alt="image-20210120173845326" style="zoom:50%;" />



# 接口用例:

## 用例位置:

在项目源代码中,包含了一个接口用例文件夹,其中存储了所有接口的测试用例,路径内容如下:

<img src="https://gitee.com/yangyuanhuyyh/MDimage/raw/master/uPic/image-20210120173040305.png" alt="image-20210120173040305" style="zoom: 33%;" />

## 测试用例:

首先保证项目已经启动了,启动成功可以看到如下输出:

![image-20210120174154358](https://gitee.com/yangyuanhuyyh/MDimage/raw/master/uPic/image-20210120174154358.png)



运行测试用例:

![image-20210120173328194](https://gitee.com/yangyuanhuyyh/MDimage/raw/master/uPic/image-20210120173328194.png)



注:因个人习惯,用例编写时从后往前的



# 接口文档:

运行项目后访问下面的链接:

http://localhost:8080/swagger-ui.html



# 交互逻辑

若不清楚交互逻辑可参考文件: <a href="https://gitee.com/yangyuanhuyyh/MDimage/raw/master/（小钱修改基础资料后，新增批注）安景农资云平台2020需求文档.docx">需求文档</a>



或登录测试系统:

地址:http://franzdage2.oicp.net:9080/a//web/index.jsp

总部管理员: zhouyb

网点管理员: 301011

超级管理员: adminlujie

生产者:51258324367

密码均为6个1







